package no.uio.inf5750.assignment2.gui.controller;

import no.uio.inf5750.assignment2.model.Course;
import no.uio.inf5750.assignment2.model.Degree;
import no.uio.inf5750.assignment2.model.Student;
import no.uio.inf5750.assignment2.service.StudentSystem;
import no.uio.inf5750.assignment2.service.impl.DefaultStudentSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

/**
 * Created by martin on 23.10.15.
 */
@Controller
public class ApiController {

    @Autowired
    StudentSystem studentSystem = new DefaultStudentSystem();

    @RequestMapping(value = "/api/student", method = RequestMethod.GET)
	@ResponseBody
	public Collection<Student> getStudents() {
		Collection<Student> students = studentSystem.getAllStudents();
		return students;
	}

    @RequestMapping(value = "/api/student/{studentId}/location", method = RequestMethod.GET)
    @ResponseBody
    public Collection<Student> setStudentLocation(@PathVariable int studentId, HttpServletRequest request) {
        String latitude = request.getParameter("latitude");
        String longitude = request.getParameter("longitude");

        studentSystem.setStudentLocation(studentId, latitude, longitude);

        Collection<Student> students = studentSystem.getAllStudents();
        return students;
    }

    @RequestMapping(value = "/api/course", method = RequestMethod.GET)
    @ResponseBody
    public Collection<Course> getCourses() {
        Collection<Course> courses = studentSystem.getAllCourses();
        return courses;
    }

    @RequestMapping(value = "/api/degree", method = RequestMethod.GET)
    @ResponseBody
    public Collection<Degree> getDegrees() {
        Collection<Degree> degrees = studentSystem.getAllDegrees();
        return degrees;
    }
}
