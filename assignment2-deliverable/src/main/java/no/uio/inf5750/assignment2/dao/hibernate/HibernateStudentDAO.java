package no.uio.inf5750.assignment2.dao.hibernate;

import no.uio.inf5750.assignment2.dao.StudentDAO;
import no.uio.inf5750.assignment2.model.Student;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;

import java.util.Collection;

/**
 * Created by martin on 23.09.15.
 */
public class HibernateStudentDAO implements StudentDAO {
    SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int saveStudent(Student student) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(student);
        return student.getId();
    }

    @Override
    public Student getStudent(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Student.class);
        criteria.add(Restrictions.eq("id", id));
        return (Student) criteria.uniqueResult();
    }

    @Override
    public Student getStudentByName(String name) {
        Session session = this.sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Student.class);
        criteria.add(Restrictions.eq("name", name));
        return (Student) criteria.uniqueResult();
    }

    @Override
    public Collection<Student> getAllStudents() {
        Session session = this.sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Student.class);
        return (Collection<Student>) criteria.list();
    }

    @Override
    public void delStudent(Student student) {
        Session session = this.sessionFactory.getCurrentSession();
        session.delete(student);
    }
}
