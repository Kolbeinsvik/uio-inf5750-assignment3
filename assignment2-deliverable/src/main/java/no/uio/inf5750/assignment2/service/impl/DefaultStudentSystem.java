package no.uio.inf5750.assignment2.service.impl;

import no.uio.inf5750.assignment2.dao.CourseDAO;
import no.uio.inf5750.assignment2.dao.DegreeDAO;
import no.uio.inf5750.assignment2.dao.StudentDAO;
import no.uio.inf5750.assignment2.model.Course;
import no.uio.inf5750.assignment2.model.Degree;
import no.uio.inf5750.assignment2.model.Student;
import no.uio.inf5750.assignment2.service.StudentSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Set;

/**
 * Created by martin on 23.09.15.
 */
@Transactional
public class DefaultStudentSystem implements StudentSystem {

    @Autowired
    CourseDAO courseDAO;

    @Autowired
    DegreeDAO degreeDAO;

    @Autowired
    StudentDAO studentDAO;

    public void setCourseDAO(CourseDAO courseDAO) {
        this.courseDAO = courseDAO;
    }

    public void setDegreeDAO(DegreeDAO degreeDAO) {
        this.degreeDAO = degreeDAO;
    }

    public void setStudentDAO(StudentDAO studentDAO) {
        this.studentDAO = studentDAO;
    }

    @Override
    public int addCourse(String courseCode, String name) {
        return courseDAO.saveCourse(new Course(courseCode, name));
    }

    @Override
    public void updateCourse(int courseId, String courseCode, String name) {
        Course course = courseDAO.getCourse(courseId);
        if(course == null) {
            // TODO log error
            return;
        }

        course.setCourseCode(courseCode);
        course.setName(name);

        courseDAO.saveCourse(course);
    }

    @Override
    public Course getCourse(int courseId) {
        return courseDAO.getCourse(courseId);
    }

    @Override
    public Course getCourseByCourseCode(String courseCode) {
        return courseDAO.getCourseByCourseCode(courseCode);
    }

    @Override
    public Course getCourseByName(String name) {
        return courseDAO.getCourseByName(name);
    }

    @Override
    public Collection<Course> getAllCourses() {
        return courseDAO.getAllCourses();
    }

    @Override
    public void delCourse(int courseId) {
        Course course = courseDAO.getCourse(courseId);

        if(course == null) {
            // TODO log error
        }

        for(Student student: this.getAllStudents()) {
            if(student.getCourses().contains(course)) {
                this.removeAttendantFromCourse(courseId, student.getId());
            }
        }

        for(Degree degree: this.getAllDegrees()) {
            if(degree.getRequiredCourses().contains(course)) {
                this.removeRequiredCourseFromDegree(degree.getId(), courseId);
            }
        }

        courseDAO.delCourse(course);
    }

    @Override
    public void addAttendantToCourse(int courseId, int studentId) {
        Course course = courseDAO.getCourse(courseId);
        Student student = studentDAO.getStudent(studentId);

        if(course == null && student == null) {
            // TODO log error
            return;
        }

        course.getAttendants().add(student);
        student.getCourses().add(course);

        courseDAO.saveCourse(course);
        studentDAO.saveStudent(student);
    }

    @Override
    public void removeAttendantFromCourse(int courseId, int studentId) {
        Course course = courseDAO.getCourse(courseId);
        Student student = studentDAO.getStudent(studentId);

        if(course == null && student == null) {
            // TODO log error
            return;
        }

        course.getAttendants().remove(student);
        student.getCourses().remove(course);

        courseDAO.saveCourse(course);
        studentDAO.saveStudent(student);
    }

    @Override
    public int addDegree(String type) {
        return degreeDAO.saveDegree(new Degree(type));
    }

    @Override
    public void updateDegree(int degreeId, String type) {
        Degree degree = degreeDAO.getDegree(degreeId);
        if(degree == null) {
            // TODO log error
            return;
        }

        degree.setType(type);
        degreeDAO.saveDegree(degree);
    }

    @Override
    public Degree getDegree(int degreeId) {
        return degreeDAO.getDegree(degreeId);
    }

    @Override
    public Degree getDegreeByType(String type) {
        return degreeDAO.getDegreeByType(type);
    }

    @Override
    public Collection<Degree> getAllDegrees() {
        return degreeDAO.getAllDegrees();
    }

    @Override
    public void delDegree(int degreeId) {
        Degree degree = degreeDAO.getDegree(degreeId);
        if(degree == null) {
            // TODO log error
            return;
        }

        for(Student student: this.getAllStudents()) {
            if(student.getDegrees().contains(degree)) {
                this.removeDegreeFromStudent(student.getId(), degreeId);
            }
        }

        degreeDAO.delDegree(degree);
    }

    @Override
    public void addRequiredCourseToDegree(int degreeId, int courseId) {
        Degree degree = degreeDAO.getDegree(degreeId);
        Course course = courseDAO.getCourse(courseId);

        if(degree == null && course == null) {
            // TODO log error
            return;
        }

        degree.getRequiredCourses().add(course);
        degreeDAO.saveDegree(degree);
    }

    @Override
    public void removeRequiredCourseFromDegree(int degreeId, int courseId) {
        Degree degree = degreeDAO.getDegree(degreeId);
        Course course = courseDAO.getCourse(courseId);

        if(degree == null && course == null) {
            // TODO log error
            return;
        }

        degree.getRequiredCourses().remove(course);
        degreeDAO.saveDegree(degree);
    }

    @Override
    public int addStudent(String name) {
        return studentDAO.saveStudent(new Student(name));
    }

    @Override
    public void updateStudent(int studentId, String name) {
        Student student = studentDAO.getStudent(studentId);
        if(student == null) {
            // TODO log error
            return;
        }

        student.setName(name);
        studentDAO.saveStudent(student);
    }


    @Override
    public void setStudentLocation(int studentId, String latitude, String longitude) {
        Student student = studentDAO.getStudent(studentId);

        if(student == null) {
            return;
        }

        student.setLongitude(longitude);
        student.setLatitude(latitude);

        studentDAO.saveStudent(student);
    }

    @Override
    public Student getStudent(int studentId) {
        return studentDAO.getStudent(studentId);
    }

    @Override
    public Student getStudentByName(String name) {
        return studentDAO.getStudentByName(name);
    }

    @Override
    public Collection<Student> getAllStudents() {
        return studentDAO.getAllStudents();
    }

    @Override
    public void delStudent(int studentId) {
        Student student = studentDAO.getStudent(studentId);

        if(student == null) {
            // TODO log error
            return;
        }

        for(Course course: this.getAllCourses()) {
            if(course.getAttendants().contains(student)) {
                this.removeAttendantFromCourse(course.getId(), studentId);
            }
        }

        studentDAO.delStudent(student);
    }

    @Override
    public void addDegreeToStudent(int studentId, int degreeId) {
        Degree degree = degreeDAO.getDegree(degreeId);
        Student student = studentDAO.getStudent(studentId);

        if(degree == null && student == null) {
            // TODO log error
            return;
        }

        student.getDegrees().add(degree);
        studentDAO.saveStudent(student);
    }

    @Override
    public void removeDegreeFromStudent(int studentId, int degreeId) {
        Degree degree = degreeDAO.getDegree(degreeId);
        Student student = studentDAO.getStudent(studentId);

        if(degree == null && student == null) {
            // TODO log error
            return;
        }

        student.getDegrees().remove(degree);
        studentDAO.saveStudent(student);
    }

    @Override
    public boolean studentFulfillsDegreeRequirements(int studentId, int degreeId) {
        Degree degree = degreeDAO.getDegree(degreeId);
        Student student = studentDAO.getStudent(studentId);

        if(degree == null && student == null) {
            // TODO log error
            return false;
        }

        Set<Course> requiredCourses = degree.getRequiredCourses();
        boolean fulfillsRequirements = student.getCourses().containsAll(requiredCourses);

        return fulfillsRequirements;
    }
}
