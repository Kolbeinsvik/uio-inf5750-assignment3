package no.uio.inf5750.assignment2.dao.hibernate;

import no.uio.inf5750.assignment2.dao.CourseDAO;
import no.uio.inf5750.assignment2.model.Course;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;

import java.util.Collection;

/**
 * Created by martin on 23.09.15.
 */
public class HibernateCourseDAO implements CourseDAO {
    SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int saveCourse(Course course) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(course);
        return course.getId();
    }

    @Override
    public Course getCourse(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Course.class);
        criteria.add(Restrictions.eq("id", id));
        return (Course) criteria.uniqueResult();
    }

    @Override
    public Course getCourseByCourseCode(String courseCode) {
        Session session = this.sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Course.class);
        criteria.add(Restrictions.eq("courseCode", courseCode));
        return (Course) criteria.uniqueResult();
    }

    @Override
    public Course getCourseByName(String name) {
        Session session = this.sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Course.class);
        criteria.add(Restrictions.eq("name", name));
        return (Course) criteria.uniqueResult();
    }

    @Override
    public Collection<Course> getAllCourses() {
        Session session = this.sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Course.class);
        return (Collection<Course>) criteria.list();
    }

    @Override
    public void delCourse(Course course) {
        Session session = this.sessionFactory.getCurrentSession();
        session.delete(course);
    }
}
