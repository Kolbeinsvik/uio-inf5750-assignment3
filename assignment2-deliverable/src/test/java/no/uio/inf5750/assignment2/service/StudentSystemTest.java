package no.uio.inf5750.assignment2.service;

import no.uio.inf5750.assignment2.model.Course;
import no.uio.inf5750.assignment2.model.Degree;
import no.uio.inf5750.assignment2.model.Student;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by martin on 24.09.15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/META-INF/assignment2/beans.xml" })
@Transactional
public class StudentSystemTest {

    @Autowired
    StudentSystem studentSystem;

    String exampleCourseCode1;
    String exampleCourseCode2;
    String exampleCourseCode3;

    String exampleCourseName1;
    String exampleCourseName2;
    String exampleCourseName3;

    String exampleDegreeType1;
    String exampleDegreeType2;
    String exampleDegreeType3;

    String exampleStudentName1;
    String exampleStudentName2;
    String exampleStudentName3;


    @Before
    public void init() throws Exception {
        exampleCourseCode1 = "INF1010";
        exampleCourseCode2 = "INF2020";
        exampleCourseCode3 = "INF3030";

        exampleCourseName1 = "INF ti-ti";
        exampleCourseName2 = "INF tjue-tjue";
        exampleCourseName3 = "INF tretti-tretti";

        exampleDegreeType1 = "Bachelor";
        exampleDegreeType2 = "Master";
        exampleDegreeType3 = "PhD";

        exampleStudentName1 = "Per Student";
        exampleStudentName2 = "Kari Student";
        exampleStudentName3 = "Askeladden";
    }


    @Test
    public void testAddCourse() {
        int savedCourseId = studentSystem.addCourse(exampleCourseCode1, exampleCourseName1);

        Assert.assertNotNull(savedCourseId);
    }

    @Test
    public void testUpdateCourse() {
        int savedCourseId = studentSystem.addCourse(exampleCourseCode1, exampleCourseName1);
        studentSystem.updateCourse(savedCourseId, exampleCourseCode2, exampleCourseName2);

        Course dbCourse = studentSystem.getCourse(savedCourseId);

        Assert.assertEquals(savedCourseId, dbCourse.getId());
        Assert.assertEquals(exampleCourseCode2, dbCourse.getCourseCode());
        Assert.assertEquals(exampleCourseName2, dbCourse.getName());
    }

    @Test
    public void testGetCourse() {
        int savedCourseId = studentSystem.addCourse(exampleCourseCode1, exampleCourseName1);
        Course dbCourse = studentSystem.getCourse(savedCourseId);

        Assert.assertEquals(savedCourseId, dbCourse.getId());
        Assert.assertEquals(exampleCourseCode1, dbCourse.getCourseCode());
        Assert.assertEquals(exampleCourseName1, dbCourse.getName());
    }

    @Test
    public void testGetCourseByCourseCode() {
        int savedCourseId = studentSystem.addCourse(exampleCourseCode1, exampleCourseName1);
        Course dbCourse = studentSystem.getCourseByCourseCode(exampleCourseCode1);

        Assert.assertEquals(savedCourseId, dbCourse.getId());
        Assert.assertEquals(exampleCourseCode1, dbCourse.getCourseCode());
        Assert.assertEquals(exampleCourseName1, dbCourse.getName());
    }

    @Test
    public void testGetCourseByName() {
        int savedCourseId = studentSystem.addCourse(exampleCourseCode1, exampleCourseName1);
        Course dbCourse = studentSystem.getCourseByName(exampleCourseName1);

        Assert.assertEquals(savedCourseId, dbCourse.getId());
        Assert.assertEquals(exampleCourseCode1, dbCourse.getCourseCode());
        Assert.assertEquals(exampleCourseName1, dbCourse.getName());
    }

    @Test
    public void testGetAllCourses() {
        studentSystem.addCourse(exampleCourseCode1, exampleCourseName1);
        studentSystem.addCourse(exampleCourseCode2, exampleCourseName2);
        studentSystem.addCourse(exampleCourseCode3, exampleCourseName3);

        int courseCount = studentSystem.getAllCourses().size();
        Assert.assertEquals(courseCount, 3);
    }

    @Test
    public void testDelCourse() {
        int savedCourseId = studentSystem.addCourse(exampleCourseCode1, exampleCourseName1);

        Course dbCourseBeforeDeletion = studentSystem.getCourse(savedCourseId);
        Assert.assertNotNull(dbCourseBeforeDeletion);

        studentSystem.delCourse(savedCourseId);

        Course dbCourseAfterDeletion = studentSystem.getCourse(savedCourseId);
        Assert.assertNull(dbCourseAfterDeletion);
    }

    @Test
    public void testDelCourseWithAttendants() {
        int savedCourseId = studentSystem.addCourse(exampleCourseCode1, exampleCourseName1);
        int savedStudentId1 = studentSystem.addStudent(exampleStudentName1);
        int savedStudentId2 = studentSystem.addStudent(exampleStudentName2);
        int savedStudentId3 = studentSystem.addStudent(exampleStudentName3);

        studentSystem.addAttendantToCourse(savedCourseId, savedStudentId1);
        studentSystem.addAttendantToCourse(savedCourseId, savedStudentId2);
        studentSystem.addAttendantToCourse(savedCourseId, savedStudentId3);

        Course dbCourseBeforeDeletion = studentSystem.getCourse(savedCourseId);
        Assert.assertNotNull(dbCourseBeforeDeletion);

        studentSystem.delCourse(savedCourseId);

        Course dbCourseAfterDeletion = studentSystem.getCourse(savedCourseId);
        Assert.assertNull(dbCourseAfterDeletion);
    }

    @Test
    public void testAddAttendantsToCourse() {
        int savedCourseId = studentSystem.addCourse(exampleCourseCode1, exampleCourseName1);
        int savedStudentId1 = studentSystem.addStudent(exampleStudentName1);
        int savedStudentId2 = studentSystem.addStudent(exampleStudentName2);
        int savedStudentId3 = studentSystem.addStudent(exampleStudentName3);

        studentSystem.addAttendantToCourse(savedCourseId, savedStudentId1);
        studentSystem.addAttendantToCourse(savedCourseId, savedStudentId2);
        studentSystem.addAttendantToCourse(savedCourseId, savedStudentId3);

        Course dbCourse = studentSystem.getCourse(savedCourseId);

        int attendantCount = dbCourse.getAttendants().size();
        Assert.assertEquals(attendantCount, 3);
    }

    @Test
    public void testRemoveAttendantsFromCourse() {
        int savedCourseId = studentSystem.addCourse(exampleCourseCode1, exampleCourseName1);
        int savedStudentId1 = studentSystem.addStudent(exampleStudentName1);
        int savedStudentId2 = studentSystem.addStudent(exampleStudentName2);
        int savedStudentId3 = studentSystem.addStudent(exampleStudentName3);

        studentSystem.addAttendantToCourse(savedCourseId, savedStudentId1);
        studentSystem.addAttendantToCourse(savedCourseId, savedStudentId2);
        studentSystem.addAttendantToCourse(savedCourseId, savedStudentId3);

        studentSystem.removeAttendantFromCourse(savedCourseId, savedStudentId1);
        studentSystem.removeAttendantFromCourse(savedCourseId, savedStudentId2);
        studentSystem.removeAttendantFromCourse(savedCourseId, savedStudentId3);

        Course dbCourse = studentSystem.getCourse(savedCourseId);

        int attendantCount = dbCourse.getAttendants().size();
        Assert.assertEquals(attendantCount, 0);
    }




    @Test
    public void testAddDegree() {
        int savedDegreeId = studentSystem.addDegree(exampleDegreeType1);

        Assert.assertNotNull(savedDegreeId);
    }

    @Test
    public void testGetDegree() {
        int savedDegreeId = studentSystem.addDegree(exampleDegreeType1);
        Degree dbDegree = studentSystem.getDegree(savedDegreeId);

        Assert.assertEquals(savedDegreeId, dbDegree.getId());
        Assert.assertEquals(exampleDegreeType1, dbDegree.getType());
    }

    @Test
    public void testUpdateDegree() {
        int savedDegreeId = studentSystem.addDegree(exampleDegreeType1);
        studentSystem.updateDegree(savedDegreeId, exampleDegreeType2);

        Degree dbDegree = studentSystem.getDegree(savedDegreeId);

        Assert.assertEquals(savedDegreeId, dbDegree.getId());
        Assert.assertEquals(exampleDegreeType2, dbDegree.getType());
    }

    @Test
    public void testGetDegreeByType() {
        int savedDegreeId = studentSystem.addDegree(exampleDegreeType1);
        Degree dbDegree = studentSystem.getDegreeByType(exampleDegreeType1);

        Assert.assertEquals(savedDegreeId, dbDegree.getId());
        Assert.assertEquals(exampleDegreeType1, dbDegree.getType());
    }

    @Test
    public void testGetAllDegrees() {
        studentSystem.addDegree(exampleDegreeType1);
        studentSystem.addDegree(exampleDegreeType2);
        studentSystem.addDegree(exampleDegreeType3);

        int courseCount = studentSystem.getAllDegrees().size();
        Assert.assertEquals(courseCount, 3);
    }

    @Test
    public void testDelDegree() {
        int savedDegreeId = studentSystem.addDegree(exampleDegreeType1);

        Degree dbDegreeBeforeDeletion = studentSystem.getDegree(savedDegreeId);
        Assert.assertNotNull(dbDegreeBeforeDeletion);

        studentSystem.delDegree(savedDegreeId);

        Degree dbDegreeAfterDeletion = studentSystem.getDegree(savedDegreeId);
        Assert.assertNull(dbDegreeAfterDeletion);
    }

    @Test
    public void testAddRequiredCourseToDegree() {
        int savedDegreeId = studentSystem.addDegree(exampleDegreeType1);

        int savedCourseId1 = studentSystem.addCourse(exampleCourseCode1, exampleCourseName1);
        int savedCourseId2 = studentSystem.addCourse(exampleCourseCode2, exampleCourseName2);
        int savedCourseId3 = studentSystem.addCourse(exampleCourseCode3, exampleCourseName3);

        studentSystem.addRequiredCourseToDegree(savedDegreeId, savedCourseId1);
        studentSystem.addRequiredCourseToDegree(savedDegreeId, savedCourseId2);
        studentSystem.addRequiredCourseToDegree(savedDegreeId, savedCourseId3);

        Degree dbDegree = studentSystem.getDegree(savedDegreeId);

        int requiredCoursesCount = dbDegree.getRequiredCourses().size();
        Assert.assertEquals(requiredCoursesCount, 3);
    }

    @Test
    public void testRemoveRequiredCourseFromDegree() {
        int savedDegreeId = studentSystem.addDegree(exampleDegreeType1);

        int savedCourseId1 = studentSystem.addCourse(exampleCourseCode1, exampleCourseName1);
        int savedCourseId2 = studentSystem.addCourse(exampleCourseCode2, exampleCourseName2);
        int savedCourseId3 = studentSystem.addCourse(exampleCourseCode3, exampleCourseName3);

        studentSystem.addRequiredCourseToDegree(savedDegreeId, savedCourseId1);
        studentSystem.addRequiredCourseToDegree(savedDegreeId, savedCourseId2);
        studentSystem.addRequiredCourseToDegree(savedDegreeId, savedCourseId3);

        studentSystem.removeRequiredCourseFromDegree(savedDegreeId, savedCourseId1);
        studentSystem.removeRequiredCourseFromDegree(savedDegreeId, savedCourseId2);
        studentSystem.removeRequiredCourseFromDegree(savedDegreeId, savedCourseId3);

        Degree dbDegree = studentSystem.getDegree(savedDegreeId);

        int requiredCoursesCount = dbDegree.getRequiredCourses().size();
        Assert.assertEquals(requiredCoursesCount, 0);
    }




    @Test
    public void testAddStudent() {
        int savedStudentId = studentSystem.addStudent(exampleStudentName1);

        Assert.assertNotNull(savedStudentId);
    }

    @Test
    public void testUpdateStudent() {
        int savedStudentId = studentSystem.addStudent(exampleStudentName1);
        studentSystem.updateStudent(savedStudentId, exampleStudentName2);

        Student dbStudent = studentSystem.getStudent(savedStudentId);

        Assert.assertEquals(savedStudentId, dbStudent.getId());
        Assert.assertEquals(exampleStudentName2, dbStudent.getName());
    }

    @Test
    public void testSetStudentGeoData() {
        String latitude = "51.5033630";
        String longitude = "-0.1276250";

        int savedStudentId = studentSystem.addStudent(exampleStudentName1);
        studentSystem.setStudentLocation(savedStudentId, latitude, longitude);

        Student dbStudent = studentSystem.getStudent(savedStudentId);

        Assert.assertEquals(dbStudent.getLatitude(), latitude);
        Assert.assertEquals(dbStudent.getLongitude(), longitude);
    }

    @Test
    public void testGetStudent() {
        int savedStudentId = studentSystem.addStudent(exampleStudentName1);
        Student dbStudent = studentSystem.getStudent(savedStudentId);

        Assert.assertEquals(savedStudentId, dbStudent.getId());
        Assert.assertEquals(exampleStudentName1, dbStudent.getName());
    }

    @Test
    public void testGetStudentByType() {
        int savedStudentId = studentSystem.addStudent(exampleStudentName1);
        Student dbStudent = studentSystem.getStudentByName(exampleStudentName1);

        Assert.assertEquals(savedStudentId, dbStudent.getId());
        Assert.assertEquals(exampleStudentName1, dbStudent.getName());
    }

    @Test
    public void testGetAllStudents() {
        studentSystem.addStudent(exampleStudentName1);
        studentSystem.addStudent(exampleStudentName2);
        studentSystem.addStudent(exampleStudentName3);

        int courseCount = studentSystem.getAllStudents().size();
        Assert.assertEquals(courseCount, 3);
    }

    @Test
    public void testDelStudent() {
        int savedStudentId = studentSystem.addStudent(exampleStudentName1);

        Student dbStudentBeforeDeletion = studentSystem.getStudent(savedStudentId);
        Assert.assertNotNull(dbStudentBeforeDeletion);

        studentSystem.delStudent(savedStudentId);

        Student dbStudentAfterDeletion = studentSystem.getStudent(savedStudentId);
        Assert.assertNull(dbStudentAfterDeletion);
    }

    @Test
    public void testAddDegreeToStudent() {
        int savedStudentId = studentSystem.addStudent(exampleStudentName1);
        int savedDegreeId1 = studentSystem.addDegree(exampleDegreeType1);
        int savedDegreeId2 = studentSystem.addDegree(exampleDegreeType2);
        int savedDegreeId3 = studentSystem.addDegree(exampleDegreeType3);

        studentSystem.addDegreeToStudent(savedStudentId, savedDegreeId1);
        studentSystem.addDegreeToStudent(savedStudentId, savedDegreeId2);
        studentSystem.addDegreeToStudent(savedStudentId, savedDegreeId3);

        Student dbStudent = studentSystem.getStudent(savedStudentId);

        int requiredDegreesCount = dbStudent.getDegrees().size();
        Assert.assertEquals(requiredDegreesCount, 3);
    }

    @Test
    public void testRemoveDegreFromStudent() {
        int savedStudentId = studentSystem.addStudent(exampleStudentName1);
        int savedDegreeId1 = studentSystem.addDegree(exampleDegreeType1);
        int savedDegreeId2 = studentSystem.addDegree(exampleDegreeType2);
        int savedDegreeId3 = studentSystem.addDegree(exampleDegreeType3);

        studentSystem.addDegreeToStudent(savedStudentId, savedDegreeId1);
        studentSystem.addDegreeToStudent(savedStudentId, savedDegreeId2);
        studentSystem.addDegreeToStudent(savedStudentId, savedDegreeId3);

        studentSystem.removeDegreeFromStudent(savedStudentId, savedDegreeId1);
        studentSystem.removeDegreeFromStudent(savedStudentId, savedDegreeId2);
        studentSystem.removeDegreeFromStudent(savedStudentId, savedDegreeId3);

        Student dbStudent = studentSystem.getStudent(savedStudentId);

        int requiredDegreesCount = dbStudent.getDegrees().size();
        Assert.assertEquals(requiredDegreesCount, 0);
    }

    @Test
    public void testStudentFulfillsDegreeRequirements() {
        int savedStudentId = studentSystem.addStudent(exampleStudentName1);
        int savedDegreeId = studentSystem.addDegree(exampleDegreeType1);

        int savedCourseId1 = studentSystem.addCourse(exampleCourseCode1, exampleCourseName1);
        int savedCourseId2 = studentSystem.addCourse(exampleCourseCode2, exampleCourseName2);
        int savedCourseId3 = studentSystem.addCourse(exampleCourseCode3, exampleCourseName3);

        studentSystem.addRequiredCourseToDegree(savedDegreeId, savedCourseId1);
        studentSystem.addRequiredCourseToDegree(savedDegreeId, savedCourseId2);
        studentSystem.addRequiredCourseToDegree(savedDegreeId, savedCourseId3);

        studentSystem.addAttendantToCourse(savedCourseId1, savedStudentId);
        studentSystem.addAttendantToCourse(savedCourseId2, savedStudentId);
        studentSystem.addAttendantToCourse(savedCourseId3, savedStudentId);

        boolean studentFulfillsDegreeRequirements = studentSystem.studentFulfillsDegreeRequirements(savedStudentId, savedDegreeId);

        Assert.assertTrue(studentFulfillsDegreeRequirements);
    }
}