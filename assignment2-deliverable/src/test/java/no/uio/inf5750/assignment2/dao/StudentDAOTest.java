package no.uio.inf5750.assignment2.dao;

import no.uio.inf5750.assignment2.dao.hibernate.HibernateStudentDAO;
import no.uio.inf5750.assignment2.model.Student;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by martin on 24.09.15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/META-INF/assignment2/beans.xml" })
@Transactional
public class StudentDAOTest {

    @Autowired
    StudentDAO studentDAO;

    Student exampleStudent1;

    public void setStudentDAO(StudentDAO studentDAO) {
        this.studentDAO = studentDAO;
    }

    @Before
    public void init() throws Exception {
        exampleStudent1 = new Student("Studentis akademis");
    }

    @Test
    public void testSaveStudent() {
        studentDAO.saveStudent(exampleStudent1);
        Student dbStudent = studentDAO.getStudent(exampleStudent1.getId());

        Assert.assertNotNull(dbStudent);
    }

    @Test
    public void testGetStudent() {
        studentDAO.saveStudent(exampleStudent1);
        Student dbStudent = studentDAO.getStudent(exampleStudent1.getId());

        boolean isTheSameStudent = exampleStudent1.equals(dbStudent);
        Assert.assertTrue(isTheSameStudent);
    }

    @Test
    public void testGetStudentByName() {
        studentDAO.saveStudent(exampleStudent1);
        Student dbStudent = studentDAO.getStudentByName(exampleStudent1.getName());

        boolean isTheSameStudent = exampleStudent1.equals(dbStudent);
        Assert.assertTrue(isTheSameStudent);
    }

    @Test
    public void testGetAllStudents() {
        Student exampleDbStudent1 = new Student("Gandalf blåbærtur");
        Student exampleDbStudent2 = new Student("Fluffy mcflurry");
        Student exampleDbStudent3 = new Student("laksus flaksus");

        studentDAO.saveStudent(exampleDbStudent1);
        studentDAO.saveStudent(exampleDbStudent2);
        studentDAO.saveStudent(exampleDbStudent3);

        boolean containsDbStudent1 = studentDAO.getAllStudents().contains(exampleDbStudent1);
        boolean containsDbStudent2 = studentDAO.getAllStudents().contains(exampleDbStudent2);
        boolean containsDbStudent3 = studentDAO.getAllStudents().contains(exampleDbStudent3);

        Assert.assertTrue(containsDbStudent1);
        Assert.assertTrue(containsDbStudent2);
        Assert.assertTrue(containsDbStudent3);
    }

    @Test
    public void testDelStudent() {
        studentDAO.saveStudent(exampleStudent1);

        Student dbStudentBeforeDeletion = studentDAO.getStudent(exampleStudent1.getId());
        Assert.assertNotNull(dbStudentBeforeDeletion);

        studentDAO.delStudent(exampleStudent1);

        Student dbStudentAfterDeletion = studentDAO.getStudent(exampleStudent1.getId());
        Assert.assertNull(dbStudentAfterDeletion);
    }
}
